#!/usr/bin/python


"""
    Starter code for the evaluation mini-project.
    Start by copying your trained/tested POI identifier from
    that which you built in the validation mini-project.

    This is the second step toward building your POI identifier!

    Start by loading/formatting the data...
"""

import pickle

from sklearn.metrics import confusion_matrix, precision_score, recall_score
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier

from tools.feature_format import featureFormat, targetFeatureSplit

data_dict = pickle.load(open("../final_project/final_project_dataset.pkl", "rb"))

# add more features to features_list!
features_list = ["poi", "salary"]

data = featureFormat(data_dict, features_list, sort_keys='../tools/python2_lesson14_keys.pkl')
labels, features = targetFeatureSplit(data)


# your code goes here
# Split data into training and testing
features_train, features_test, labels_train, labels_test = train_test_split(features, labels, test_size=0.3,
                                                                            random_state=42)

# Fit data with sklearn decision trees algorithm

clf = DecisionTreeClassifier()
clf.fit(features_train, labels_train)

print('score:', clf.score(features_test, labels_test))

pred = clf.predict(features_test)
cm = confusion_matrix(labels_test, pred)
print(cm)
# [[21  4]
#  [ 4  0]]
# How many POIs are in the test set for your POI identifier? == 4??
# How many people in total = 29
# You just finished Accuracy of a Biased Identifier = 25/29 (if all predicted were 0 (not poi))
# Number Of True Positives = 0
prec_score = precision_score(labels_test, pred)
print('precision_score:', prec_score)  # precision_score: 0.0
rec_score = recall_score(labels_test, pred)
print('recall_score:', rec_score)  # recall_score: 0.0

# # ###
# predictions = [0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1]
# true_labels = [0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0]
#
# confusion = confusion_matrix(true_labels, predictions)
# print(confusion)
# print('precision: ', precision_score(true_labels, predictions))
# print('recall: ', recall_score(true_labels, predictions))