#!/usr/bin/python

import pickle
import sys
from pprint import pprint

import matplotlib.pyplot
sys.path.append("../tools/")
from tools.feature_format import featureFormat, targetFeatureSplit


### read in data dictionary, convert to numpy array
data_dict = pickle.load(open("../final_project/final_project_dataset.pkl", "rb"))
features = ["salary", "bonus"]

outlier = 'TOTAL'
data_dict.pop(outlier, 0)


data = featureFormat(data_dict, features)


# print(name)
# pprint(data_dict[name])

### your code below

for point in data:
    salary = point[0]
    bonus = point[1]
    matplotlib.pyplot.scatter( salary, bonus )

matplotlib.pyplot.xlabel("salary")
matplotlib.pyplot.ylabel("bonus")
matplotlib.pyplot.show()


max_salary = []
for n in data_dict:
    if isinstance(data_dict[n]['salary'], str):
        continue
    else:
        max_salary.append({'name': n,
                           'salary': data_dict[n]['salary']})

max_salary = sorted(max_salary, key=lambda k: k['salary'])
pprint(max_salary[-2])
pprint(max_salary[-1])
