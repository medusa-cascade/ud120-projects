#!/usr/bin/python


def outlierCleaner(predictions, ages, net_worths):
    """
        Clean away the 10% of points that have the largest
        residual errors (difference between the prediction
        and the actual net worth).

        Return a list of tuples named cleaned_data where 
        each tuple is of the form (age, net_worth, error).
    """
    
    cleaned_data = []

    ### your code goes here
    errors = []
    for i, p_v in enumerate(predictions):
        errors.append({'error': abs(1-p_v[0]/net_worths[i][0]),
                       'data': (ages[i], net_worths[i], abs(1-p_v[0]/net_worths[i][0]))})
    errors = sorted(errors, key=lambda k: k['error'])[:81]

    for d in errors:
        cleaned_data.append(d['data'])


    return cleaned_data

