#!/usr/bin/python

""" 
    This is the code to accompany the Lesson 2 (SVM) mini-project.

    Use a SVM to identify emails from the Enron corpus by their authors:    
    Sara has label 0
    Chris has label 1
"""
    
# import sys
from time import time
# sys.path.append("../tools/")
from sklearn.metrics import accuracy_score

from tools.email_preprocess import preprocess


### features_train and features_test are the features for the training
### and testing datasets, respectively
### labels_train and labels_test are the corresponding item labels
features_train, features_test, labels_train, labels_test = preprocess()

### smaller train set
# features_train = features_train[:int(len(features_train)/100)]
# labels_train = labels_train[:int(len(labels_train)/100)]



#########################################################
### your code goes here ###
from sklearn import svm
clf = svm.SVC(kernel='rbf', C=10000.)

t0 = time()
clf.fit(features_train, labels_train)
print("training time:", round(time()-t0, 3), "s")

t1 = time()
pred = clf.predict(features_test)
# change numpy.array into list and count occurrence of Chris (1)
print(list(pred).count(1))

# print("10: {}\n26: {}\n50: {}".format(*pred))
# print("predicting time:", round(time()-t1, 3), "s")
#
# print("accuracy:", clf.score(features_test, labels_test))
# print("with metrics:", accuracy_score(pred, labels_test))
#########################################################


